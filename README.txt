POXY is a set of command-line tools and Python modules 
intended to help with molecular dynamics simulation of Si oxidation. 
Please, consult the online documentation for details:
(http://poxy.readthedocs.org/en/latest/about.html).
The code is available under The MIT license.
