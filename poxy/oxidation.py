# Copyright (C) 2015, Stanislav Markov, The University of Hong Kong
# See accompanying license files for details.

from __future__ import print_function
import numpy as np
import logging
from random import uniform
from math import sqrt, pi, cos, sin
from ase import Atom
from ase.calculators.neighborlist import NeighborList
from ase.data import covalent_radii, atomic_numbers
from ase.io import write
from poxy.utils.misc import tostr, interatomic_dist, get_overlapped_atoms
from poxy.utils.misc import dist_from_point, dist_from_axis, dist_from_plane
from poxy.utils.misc import dist_pt2pt, dist_pt2axis, dist_pt2plane

avbondlen_Si_O = 1.6 # [A] average bond-length in vitreous silica
rcut_Si = 1.3875 # [A] half between Si-Si 1st neighbour in diamon-Si (2.35 A)
                 #     and Si-O-Si 2nd neighbour in SiO2 (3.2A)
rcut_O  = 0.75   # [A] somewhat arbitrary... a bit less than rcov_O*rcut_Si/rcov_Si
                 #     but a bit larger than: ((Si_O(quartz)-rcut_Si) + O-Si-O(quartz))/4.
                 #     where Si-O is 1.6 A and O-Si-O is 2.7 A

struct_ref_dict = {'dot': (0,0,0), 
                   'wire': ((0,0,0), (1,0,0)),
                   'film': ((0,0,0), (1,0,0), (0,1,0))}

def set_oxygen_position(at1, at2, cell, offset):
    """Return the calculated position of the oxygen, so that it is at the nominal
    distance from Si atoms and in the mid-plane normal to the Si-Si bond.
    
    Parameters:
        at1, at2 : atom objects (obj.position is mandatory)
            Two atoms forming a bond. We return a position that is
            on the transverse mid-plane and avbondlen_Si_O apart from
            either of at1/2.
        cell: array like 3x3 (i.e. list of three vectors)
            unit cell of the atomic structure embedding at1/2
        offset:
            offset of the at2 in terms of unit cell vectors, 
            e.g. [1,0,0] means next that at2 is actually in the 
            next cell along X.

    Returns:
        float:
            The calculated position of the oxygen. Note that a random phase is
            drawn to position the oxygen arbitrary in the mid-plane normal to
            the bond, subject to distance constraint. 

    TODO:
        May be it will be good to seed the random generator for the
        sake of repeatability if desired for whatever purpose.
        Apparently the seed is globally determined by current time upon
        import of the 'random' module. Manual seed should also be 
        supplied globally for a given run, not per oxygen.
    """
    p1, p2 = at1.position, at2.position + np.dot(cell, offset)
    p1p2 = np.linalg.norm(p1-p2)
    origin = 0.5 * (p1 + p2)
    # print ("O'", origin)
    # print ("V", p2-p1)
    # print ("|V|/2", p1p2/2.)
    radius = sqrt(avbondlen_Si_O**2 - (0.5*p1p2)**2)
    # draw a radnom phase and convert to cartesian in rotated coordinate system
    azimuth = uniform(0, 2*pi)
    _x = radius * cos(azimuth)
    _y = radius * sin(azimuth)
    assert np.allclose(_x**2 + _y**2 + (0.5*p1p2)**2, avbondlen_Si_O**2)
    # define the rotated coordinate system in terms of original coordinates
    # 1. get a normalised vector from the Si-Si bond
    e3 = p2 - p1
    e3 = e3/np.linalg.norm(e3)
    # 2. Define an orthogonal vector e:
    #    use cross product with an arbitrary vector, to 
    #    avoid various checks for zero components;
    #    Use one of the coordinate axes for second vector,
    #    But make sure it is not collinear ()
    eref = [[1,0,0],[0,1,0],[0,0,1]]
    for ee in eref:
        # below we ensure our vector is at > 45 degrees respect to axis
        if abs(np.dot(e3, ee)) < 0.7: #
            e1 = np.cross(e3, ee)
            e1 = e1/np.linalg.norm(e1)
            break
    # print ("e'_1", e1)
    # 3. define the third orthogonal vector as a cross product
    #    of other two unit vectors => no need to normalise
    e2 = np.cross(e3, e1)
    # e2 = e2/np.linalg.norm(e2)
    assert np.allclose(np.linalg.norm(e1), 1.), np.linalg.norm(e1)
    assert np.allclose(np.linalg.norm(e2), 1.), np.linalg.norm(e2)
    assert np.allclose(np.linalg.norm(e3), 1.), np.linalg.norm(e3)
    assert np.allclose(np.dot(e1, e3), 0.), np.dot(e1, e3)
    assert np.allclose(np.dot(e1, e2), 0.), np.dot(e1, e2)
    assert np.allclose(np.dot(e2, e3), 0.), np.dot(e2, e3)
    # get position of oxygen in original coordinates, including rigin shift
    R = np.array([e1, e2, e3])
    # print ("R:\n", R)
    pO = np.dot(np.transpose(R), np.transpose([_x,_y, 0])) + origin
    # print ("P_O", pO)
    assert np.allclose(np.linalg.norm(pO - p1), avbondlen_Si_O), np.linalg.norm(pO - p1)
    assert np.allclose(np.linalg.norm(pO - p2), avbondlen_Si_O), np.linalg.norm(pO - p2)
    # must check if pO is within the supercell, and fold it if not
    for i in range(3):
        if pO[i] > np.linalg.norm(cell[i]/2.):
            # print ("oxygen outside the cell. folding it back.")
            pO[i] -= np.linalg.norm(cell[i])
    return pO

def insert_oxygen_atom(atoms, struct_type='wire', struct_ref=None, 
        mode='subox', randomvelocities=False, aS='Si', nvale=4, aO='O',
        cutoffs={'Si': rcut_Si, 'O': rcut_O}, cutoff_factor=1.1,
        overlap_radius=1.0, mincorerad=0.0, minbondlen=2.25, enlarge=True):
    """Insert oxygen in the middle of a Si-Si bond, one atom at a time.

    The algorithm is as described in 
    J.Torre et al, "Study of self-limiting oxidation of silicon 
    nanoclusters by atomistic simulations," J. Appl. Phys. 92/2 1084, 2002.

    1. We find the surface (less than 4-Si coordinated) or sub-oxidised 
       (4-coordinated but with at least 1 oxygen) atoms, and label them 
       fit for oxidation.
    2. We find the farthest of the atoms fit for oxidation. Distance is
       measured relative to point/axis/plane, depending on the _stryct_type_
       being dot/wire/film.
    3. Of this farthest atom, we select the longest bond (farthest neigbour).
    4. We insert O in the mid-plane normal to the bond, at a random
       position on a circle that is equidistant from either Si atoms,
       the distance being equilibrium Si-O bond in vitreous silica.
    5. If all goes well and O is inserted, we return True; else False.

    NOTABENE: The operation changes the input _atoms_ structure!!!

    REMARKS:
        1. The routine first establishes a neighbour list of the structure.
           For large structures, building the neighbour list is slow. We
           rely here on the implementation of ASE, for the neighbour list.
           It may be advantagous to speed up the process by restricting the
           number of atoms only to a layer of given thickness below the 
           surface, but have not been tried yet.
        2. It may happen for some reason that the attempt to add oxygen
           fails, and this is when False is returned. Typically, the oxygen
           is added, but then it is found out that it overlaps (i.e. is
           within _overlap_radius_ of another oxygen. Then it is removed,
           and False returned from the routine. Annealing for a while should
           eliminate the condition leading to the overlap.
    
    Parameters:
        struct_type: string
            Type of atomic structure: one of 'dot', 'wire', 'film'.
            Assumed is that if:
            'dot' the atoms are 'centred' around the point 0,0,0,
            'wire' the atoms are 'centred' around the axis [1,0,0],
            'film' the atoms are above the plane with normal [0,0,1].
            These assumptions go into evaluating distance (needed to determine
            the 'farthest' atom from the assumed point/axis/plane of reference),
            and to evaluating core thickness.
        struct_ref: array
            The reference point (dot), axis (wire) or plane (film) for evaluating
            distance of atomic positions. If not explicitly specified, it will be
            deduced from struct_type: 
            Origin (dot), X-axis (wire), XY-plane through Origin (film).
        mode: string, Default: 'subox'
            'surface' : insert oxygen only in bonds of top surface atoms
            'dangling' : insert oxygen only in dangling bonds
            'subox': insert oxygens only in the bonds of partially oxidised Si
            'either' : insert O in either partially oxidised Si bonds, or surface, or dangling bonds
        randomvelocities: bool, Default: False
            If False, then we expect to have the velocities in the atoms object.
            In such case, the output object will keep the velocities of the old atoms,
            while the added oxygen(s) will be 'cold', i.e. null velocity.
            If True, or if the atoms.has('momenta') returns false (e.g. if atoms
            is obtained from reading a .gen file) then no velocities will appear in
            atoms object even after O addition.
        aS: string, Default: 'Si'
            Atomic symbol of the substrate atoms; this may allow alternative
            substrate to be oxidised, in the future
        nvale: int
            Number of valence electrons; In the future, this should be deduced
            somehow from aS directly. But we have a problem with compound
            substrates...
        aO: string, Default: 'O'
            Atomic symbol of the oxygen atoms; this may allow for inserting
            nitrogen or hydrogen as an alternative, in the future
        cutoffs: dictionary {"Chem.Symbol": float_cutoffradius, }
            A pair of atoms is returned if the distance between the two
            is less than the sum of their cutoffradii.
            No need to specify this for all elements.
            If None, then cutoffradius = cutoff_factor * covalentradius
        cutoff_factor: float
            cutoff_radii = cutoff_factor * covalent_radii[atoms.numbers] # defaults
            The default of 1.1 may suffice in many cases: e.g. Si-Si in diamond-Si
            is 2.35, while rcov_Si is 1.11, so 1.1*rcov_Si*2 > 2.32, but may as
            well be small, depending on analysis we want to make...
        overlap_radius: float [Angstrom]
            Defines if two O atoms overlap (if closer than overlap_radius).
            Overlapping oxygens are pruned (removed).
            The default value of 1.3 is half of O-O 2nd neighbour in the oxide.
        mincorerad: float [Angstrom]
            This is not supported yet.
            Defines the minimum radius of the Si core.  But in principle, 
            the core dimension should be measured like with respect to a 
            point/axis/plane, for dot/wire/film, respectively.
        minbondlen: float [Angstrom]
            This is not supported yet. 
            Defines the minimum Si-Si bond-length of a surface bond, in which
            O can be inserted.  If Si-Si distance is smaller, O is not inserted.
            The default value represents 0.95 of the nominal Si-Si bond in bulk Si.
            It is approximately \mu - 5\sigma (assuming sigma ~0.02).
            The anticipated effect is therefore to stop adding oxygen if compressive
            strain develops at the interface, and thus mimic the observed self-limiting
            oxidation. However, the dynamics should push the O outward even if we add
            it, at the expense of worsening the structure (probably overcoordination?)
            in the region that is expected to be stoichiometric.
        enlarge: bool
            Enlarge the unit cell so as to maintain the same vacuum buffer.
            This is necessary due to the expansion of the atomic structure as O is
            incorporated. The approach relies on a simple approximation: 
            that the volume of SiO2 corresponds to the 
            SiO2_mass_density * (nO * mO + 0.5 * nO * mSi), as in perfect SiO2, 
            while the volume of Si corresponds to
            Si_mass_density * (nSi - 0.5 * nO) * mSi,
            where nO/nSi are number of O/Si atoms, mO/mSi the corresponding masses. 
            The mass_densities are from ideal structures or experiment.
            So the corresponding increase in the volume must correspond to the
            increase in nO.
    Returns:
        ASE Atoms object
            A copy of the *atoms* structure with oxygens inserted.
    """

    logger = logging.getLogger('poxy_logger')
          
    def verbose_atom(atom, head='', tail=''):
        """Verbose string for an atom."""
        atomstr = "{:2s}{:6d}{:9.3f}{:9.3f}{:9.3f}".\
            format(atom.symbol, atom.index, atom.x, atom.y, atom.z)
        return "".join([head, atomstr, tail])
    
    def qualify_nbatoms(atoms, ix, nl, anyoffset=True):
        """Check if neighbouring atoms can be oxidised.

        We exclude:
            * O neighbours;
            * Si neighbours with which an O is shared, even if Si
              is still within a bonding distance (i.e. the Si-Si bond
              is already oxidised);
            * Si neighbours accross a periodic boundary, when the neighbour
              is in the cell with negative offset, unless _anyoffset_=True.
              This exclusion is necessary to avoid overlapping oxygens, 
              where the overlap is hidden by the fact that they are one
              cell apart.

        Parameters:
            atoms: ASE.Atoms or alike
                atoms object holding the atomic structure
            ix: int
                index of the atom of interest, whose neighbours we're
                trying to qualify
            nl: ASE NeighBourlist or alike
                the neighbourlist of the atoms object
            anyoffset: bool
                permit (True) or not (False) oxidation of a bond that
                goes accross a periodic boundary, where the offset of
                the neighbouring cell is negative.

        Returns:
            _indeces: [int]
                list of indeces of neighbours that can be oxidised
            _offsets: [int]
                corresponding offsets in terms of unit cell
        """
        at = atoms[ix]
        indices, offsets = nl.get_neighbors(ix)
        fit_indices, fit_offsets = [], []
        O_nbrs = [k for k in indices if atoms[k].symbol=='O']
        for jj, oo in zip(indices, offsets):
            # atoms[jj] is the neighbour, oo is its unit-cell offset
            at_1 = atoms[jj]
            # check if atoms[ix] and atoms[jj] have a common O-bridge already
            ind_1, offs_1 = nl.get_neighbors(jj)
            O_nbrs_1 = [k for k in ind_1 if atoms[k].symbol=='O']
            common_O = len(set(O_nbrs) & set(O_nbrs_1)) > 0
            # get distance between the atoms[ix] and atoms[jj]
            ia_dist = interatomic_dist(at, at_1, cell, oo)
            # qualify atoms[jj] as fit, if it can be oxidised
            if ((oo>= 0).all() or anyoffset)\
                and at_1.symbol==aS and not common_O: # and dist > minbondlen:
                fit_indices.append(jj)
                fit_offsets.append(oo)
                logger.debug(verbose_atom(at_1, head='    ',
                    tail=': Fit  (dist:{:6.3f} offs:{})'.
                        format(ia_dist, oo)))
            else:
                if at_1.symbol == aS and common_O:
                    logger.debug(verbose_atom(at_1, head='    ',
                        tail=': Skip (dist:{:6.3f} offs:{}), common O'.
                            format(ia_dist, oo)))
                else:
                    logger.debug(verbose_atom(at_1, head='    ',
                        tail=': Skip (dist:{:6.3f} offs:{})'.
                            format(ia_dist, oo)))
        return fit_indices, fit_offsets
            
    atomdistance = {'dot' : dist_pt2pt, 
                    'wire': dist_pt2axis,
                    'film': dist_pt2plane}
    if struct_ref is None:
        struct_ref = struct_ref_dict[struct_type]
    
    do_mode = {'surface':(True,False),
               'subox':(False,True),
               'either':(True,True),
               'dangling':(False,False)}

    if randomvelocities==False and not atoms.has('momenta'):
        # note that ASE.atoms stores momenta, not velocities!
        logger.error('You asked for initial velocities, but these are not available.')

    do_surface, do_subox = do_mode[mode]
    lenpreox = len(atoms)
    cell = atoms.get_cell()
    cutoff_radii = cutoff_factor * covalent_radii[atoms.numbers] # defaults
    for chemsymb, rcut in cutoffs.items():
        cutoff_radii[atoms.numbers==atomic_numbers[chemsymb]] = rcut
    # Below we need bothways and not self_interaction so that
    # for every atom, we get all neighbours. 
    # Note: if we set bothways=False we do not get complete list of 
    # neighbours for some atoms. When is this a problem? When the 
    # list of the most # distant atom happens to be empty.
    # The question is: would we put two oxigens on both bonds accross
    # a periodic boundary? Probably not, unless we try to add oxigen
    # without annealing (MD) or relaxing (CG) the atoms.
    # The answer will be yes for a 'layer' or 'blanket' insertion of
    # oxygen, or maybe when we try to insert many oxygens at a time.
    # Therefore we may not need to qualify the neighbour atoms by 
    # their offset.
    # Skin is set to 0, otherwise a bond will be returned even for 
    # atoms placed beyond the sum of their cutoffs_radii apart.
    nl = NeighborList(cutoffs=cutoff_radii, skin=0.0,
            bothways=True, self_interaction=False)
    nl.build(atoms)

    # Here we assume that atoms are sorted by atom types, and that the
    # O-atoms are last, in the *atoms* structure
    nOstart = len([at for at in atoms if at.symbol==aO])

    # 2a. Find which atoms can be oxidised.
    #     We look for atoms that have dangling bonds, i.e. less than 
    #     4-Si-coordinated, or atoms that are partially oxidised but 
    #     could become fully oxidised.
    surface = []
    partiallyoxidised = []
    for ix, at in enumerate(atoms):
        if at.symbol==aS:
            nbindices, nboffsets = nl.get_neighbors(ix)
            nbatoms = [atoms[i] for i in nbindices]
            logger.debug(verbose_atom(at, head="*** ", 
                    tail=" coordination {:d}".format(len(nbindices))))
            # Atom with a dangling bond means under-coordinated atom: 
            # less bonds than the number of valence electrons.
            # Make sure we do not add O to fully or over-coordinated atoms,
            # which would happen if below we use !=.
            # problem: below, we must check all neighbours are aS
            if do_surface and len(nbindices) < nvale and\
                all(nbat.symbol==aS for nbat in nbatoms):
                _indices, _offsets = qualify_nbatoms(atoms, ix, nl, anyoffset=True)
                surface.append([ix, _indices, _offsets])
            # Here we have fully- and over-coordinated Si, but oxidised.
            # Note that over-coordinated Si atoms would be if we do not
            # relax the structure sufficiently, which may lead to too many Si 
            # neighbours within 1st neighbour distance even after insertion of
            # O in one of the bonds.
            # So we should always try to equilibrate the structure after O addition.
            elif do_subox and len(nbindices) <= nvale and\
                aO in [nbat.symbol for nbat in nbatoms]:
                _indices, _offsets = qualify_nbatoms(atoms, ix, nl, anyoffset=True)
                partiallyoxidised.append([ix, _indices, _offsets])
            else:
                _indices, _offsets = [], []
                
    if do_surface:
        logger.info("Found {:n} surface atoms:".format(len(surface)))
        logger.info([a[0] for a in surface])
    if do_subox:
        logger.info("Found {:n} partially oxidised atoms:".format(len(partiallyoxidised)))
        logger.info([a[0] for a in partiallyoxidised])

              
    # 3. Decide which atom to oxidise:
    # 3a. Find the farthest atom to be oxidised
    # fitforoxidation is [atom.index, [neighbours'indices], [neigbours'offsets]]
    fitforoxidation = surface + partiallyoxidised
    assert len(fitforoxidation[0]) == 3
    
    fitforoxidation.sort(key=lambda x: 
            atomdistance[struct_type](atoms[x[0]].position, struct_ref))
    nbatoms = []
    # The index of the farthest atom from the reference pt/ax/pl is
    # the last in the list fitforoxidation; but it may happen to have no
    # neighbouring bonds that are fit for oxidation -- hence the loop below
    last = len(fitforoxidation)
    while not nbatoms:
        last -= 1
        assert last >= 0
        at = atoms[fitforoxidation[last][0]]
        nbatoms = [atoms[i] for i in fitforoxidation[last][1]]
        nboffsets = fitforoxidation[last][2]
    logger.info("Oxidising {:s}".format(verbose_atom(at)))

    # 3b. Find its longest Si-Si bond and insert oxygen in the mid-plane
    bondlen = [interatomic_dist(at, nba, cell, nbo)\
               for nba, nbo in zip(nbatoms, nboffsets)]
    logger.debug("With neigbours:")
    for nba, bl, nbo in zip(nbatoms, bondlen, nboffsets):
        logger.debug("{:s} ({:6.3f} away, offs {})".
            format(verbose_atom(nba), bl, nbo))
    # notabene: index() below will yield the first match ONLY
    longestbond = sorted(bondlen)[-1]
    farthestix = bondlen.index(longestbond)
    farthestnb = nbatoms[farthestix]
    farthestoffset = nboffsets[farthestix]

    # 3c. Place the new O atom.
    def place_oxygen():
        posO = set_oxygen_position(at, farthestnb, cell, farthestoffset)
        if atoms.has('momenta') and randomvelocities==False:
            # for now, we add cold atom (0 velocity and momentum)
            atoms.append(Atom(aO, posO, momentum=[0., 0., 0.]))
        else:
            # this is not nice as we rely on the MD engine to 
            # to initialise velocity.. if it doesn't do it for this atom
            # then it will effectively remain cold, even if other
            # atoms has velocities from a file; a global random velocities
            # for all atoms will sort this out.
            atoms.append(Atom(aO, posO))
        atO = atoms[len(atoms)-1]
        logger.info("Added {:s}, mid-bond {:6d} -- {:<6d}, apart by {:.2f}".
                    format(verbose_atom(atO), at.index, farthestnb.index, 
                        longestbond))

    # If for come reason there is an overlap, remove and try again.
    # This should not happen, but in case we have a situation where
    # the last O overlaps (even loosely so) with another O, 
    # then we prune it and try to add it again.
    # After 20 attempts, we give up, leaving it for the MD to
    # shuffle the atoms enough so that new O can be incorporated
    # without difficulty.
    # Recall that we add the O with random position on a circle.
    # By overlapping, we mean not exactly, but within overlap_radius
    # from each other.
    attemptcntr = 1
    overlapatoms = []
    maxattempts = 20
    for i in range(maxattempts):
        place_oxygen()
        overlapatoms = get_overlapped_atoms(atoms, overlap_radius, species='O')
        if len(atoms)-1 not in overlapatoms:
            break
        else:
            logger.warning("{:3d} Pruning the added O atom due to overlap: {}".\
                    format(attemptcntr, tostr(overlapatoms)))
            del atoms[-1]
        
    # This should never happen at this stage, but keep it in for now.
    if overlapatoms in overlapatoms:
        logger.error("{:3d} Pruning overlapping atoms: {}".\
                format(attemptcntr, tostr(overlapatoms)))
        del atoms[overlapatoms]

    # check what we have accomplished in the end?
    nOstop = len([at for at in atoms if at.symbol==aO])
    if nOstop >= nOstart + 1:
        success = True
        logger.info("Added {:n} {:s} atoms in total".format(nOstop-nOstart, aO))
    else:
        if nOstop == nOstart:
            success = False
            logger.error("Failed to insert O")
        else:
            logger.critical("CRITICAL ERROR: loosing oxygens during oxidation (overlapping atoms)?!!")
            sys.exit(1)
    return success

def insert_oxygen_lbl(atoms, mode='layer', aS='Si', nvale=4, aO='O', 
        radius=1.1, overlap_radius=1.0, mincorerad=0.0, minbondlen=2.25,
        verbose=False):
    """Insert oxygen in the middle of surface bonds.
    
    The algorithm is as follows: we find all surface and sub-oxide bonds
    and insert O atoms in the middle of them. 
    How we find surface/sub-oxidised bonds:

        1. We build a neighbour list and look at the atoms which are not
           fully coordinated by their own species: these are either 
           substrate surface atoms with dangling bonds (sub-coordinated), 
           or sub-surface, sub-oxidised atoms (e.g. Si connected to one
           O and three Si).

        2. All bonds of the surface/sub-oxidised atoms with other atoms
           of their own species are candidates for O insertion.

    Depending no the *mode*, we either insert O in the middle of all 
    surface bonds ('layer'), or pick up at random a surface bond and
    insert there ('atom'). In either case, it is possible to get two 
    oxygens too close (defined as closer than *overlap_radius*). One of 
    the overalapping oxygens is pruned. With *verbose*, all surface bonds,
    added and pruned oxygens are reported.
    
    REMARKS:
        1. mode='atom' is NOT implemented yet.
        2. For large structures, building the neighbour list is slow. We
           rely here on the implementation of ASE, for the neighbour list.
           It may be advantagous to speed up the process by restricting the
           number of atoms only to a layer of given thickness below the 
           surface, but have not been tried yet.
    
    Parameters:
        mode: string, Default: 'layer'
            'layer': insert oxygen over entire non-periodic surface
            'atom' : insert only one oxygen, randomly choosing a bond
        aS: string, Default: 'Si'
            Atomic symbol of the substrate atoms; this may allow alternative
            substrate to be oxidised, in the future
        nvale: int
            Number of valence electrons; In the future, this should be deduced
            somehow from aS directly. But we have a problem with compound
            substrates...
        aO: string, Default: 'O'
            Atomic symbol of the oxygen atoms; this may allow for inserting
            nitrogen or hydrogen as an alternative, in the future
        radius: float, [Angstrom]
            The fraction of covalent radii considered when building the neighbour
            list
        overlap_radius: float [Angstrom]
            Defines if two O atoms overlap (if closer than overlap_radius).
            Overlapping oxygens are pruned (removed).
            The default value of 1.3 is half of O-O 2nd neighbour in the oxide.
        mincorerad: float [Angstrom]
            Defines the minimum radius of the Si core. If the transverse position
            of an oxidising atom would fall within this radius, it is not added.
        minbondlen: float [Angstrom]
            Defines the minimum Si-Si bond-length of a surface bond, in which
            O can be inserted. If Si-Si distance is smaller, O is not inserted.
            The default value represents 0.95 of the nominal Si-Si bond in bulk Si.
            It is approximately \mu - 5\sigma (assuming sigma ~0.02).
            The anticipated effect is therefore to stop adding oxygen if compressive
            strain develops at the interface, and thus mimic the observed self-limiting
            oxidation.
        verbose: bool, Default: False
            If True, a lot of info of what atoms are added and where, and what
            atoms are pruned (e.g. due to overlapping)
    
    Returns:
        ASE Atoms object
            A copy of the *atoms* structure with oxygens inserted.
    """
          
    def verbose_atom(atom, head='', tail=''):
        """Verbose string for an atom."""
        atomstr = "{:2s}{:6d}{:9.3f}{:9.3f}{:9.3f}".\
            format(atom.symbol, atom.index, atom.x, atom.y, atom.z)
        return "".join([head, atomstr, tail])
            
    lenpreox = len(atoms)
    cell = atoms.get_cell()
    cutoffs = radius * covalent_radii[atoms.numbers]
    # Below we need bothways and not self_interaction so that
    # for every atom, we get all neighbours. Note: if we set
    # bothways=False we do not get complete list of neighbours for
    # some atoms.
    # The question is: would we put two oxigens on both bonds accross
    # a periodic boundary? Answer is yes, unless we check for the sign
    # of offsets, in the neighbour list, and ignore the ones in 
    # negative direction.
    # Skin is set to 0, otherwise a bond will be returned even for 
    # atoms placed beyond the sum of their cutoffs apart.
    nl = NeighborList(cutoffs=cutoffs, skin=0.0,
            bothways=True, self_interaction=False)
    nl.build(atoms)

    # 1. Find all bonds (this will take long for a big structure!)
    #    In the future, use the _nl_ and restrict the bond-search
    #    to the atoms that are meant to be oxidised, i.e. by 
    #    introducing some spacial criteria to reduce the atoms of interest
    bonds_to_oxidise = []
    # Here we assume that atoms are sorted by atom types, and that the
    # O-atoms are last, in the *atoms* structure
    nOstart = len([at for at in atoms if at.symbol==aO])

    # 2a. Find to atoms to oxidise.  
    #     We look for atoms that have dangling bonds, i.e. less than 
    #     4-Si-coordinated, or atoms that are partially oxidised but 
    #     could become fully oxidised.
    #     We make sure we do not insert O in bonds that are compressed
    #     too much, as defined by minbondlen.
    surface = []
    partiallyoxidised = []
    for ix, at in enumerate(atoms):
        if at.symbol==aS:
            indices, offsets = nl.get_neighbors(ix)
            # Dangling bond means under-coordinated atom: less bonds
            # than the number of valence electrons.
            # Make sure we do not add O to fully or over-coordinated atoms,
            # which would happen if below we use !=.
            if verbose:
                print(verbose_atom(at, head="*** ", tail=" with {:d} bonds".
                    format(len(indices))))
            # problem: below, we must check all neighbours are aS
            if len(indices) < nvale and\
                all(atoms[i].symbol==aS for i in indices):
                # Here we exclude bonds that go across periodic boundary
                # and are directed in the negative direction, or else,
                # we would end with two overlapping oxygens, hidden by the
                # fact that they are distant by exactly one lattice constant.
                # We also exclude Si-O bonds.
                _indices, _offsets = [], []
                for ii,jj in enumerate(indices):
                    at1 = atoms[jj]
                    dist = interatomic_dist(at, at1, cell, offsets[ii])
                    if (offsets[ii]>= 0).all() and at1.symbol==aS and dist > minbondlen:
                        _indices.append(indices[ii])
                        _offsets.append(offsets[ii])
                        bonds_to_oxidise.append(((at.index, indices[ii]), offsets[ii]))
                        if verbose:
                            print(verbose_atom(at1, head='    ', tail=': Oxidise (dist:{:6.3f})'.format(dist)))
                    else:
                        if verbose:
                            print(verbose_atom(at1, head='    ', tail=': Skip (dist:{:6.3f})'.format(dist)))
                surface.append([ix, _indices, _offsets])
            elif aO in [atoms[i].symbol for i in indices]:
                # Here we have fully- and over-coordinated Si, but oxidised.
                # Note that over-coordinated Si atoms would be if we do not
                # relax the structure sufficiently, and Si--Si bond forms even
                # with an O atom in between.
                _indices, _offsets = [], []
                for ii,jj in enumerate(indices):
                    at1 = atoms[jj]
                    dist = interatomic_dist(at, at1, cell, offsets[ii])
                    if (offsets[ii]>= 0).all() and at1.symbol==aS and dist > minbondlen:
                        _indices.append(indices[ii])
                        _offsets.append(offsets[ii])
                        bonds_to_oxidise.append(((at.index, indices[ii]), offsets[ii]))
                        if verbose:
                            print(verbose_atom(at1, head='    ', tail=': Oxidise (dist:{:6.3f})'.format(dist)))
                    else:
                        if verbose:
                            print(verbose_atom(at1, head='    ', tail=': Skip (dist:{:6.3f})'.format(dist)))
                partiallyoxidised.append([ix, _indices, _offsets])
                
    print("Found {:n} surface atoms:".format(len(surface)))
    print("Found {:n} partially oxidised atoms:".format(len(partiallyoxidised)))

       
    # 2c. Prune duplicates from the bonds_to_oxidise. These are 
    #     at1,at2 and at2,at1 pairs.
    print ("Found {} bonds to oxidise (including duplicates)".
        format(len(bonds_to_oxidise)))
    seen = set()
    seen_bonds = []
    for bond, offset in bonds_to_oxidise:
        if bond in seen or bond[::-1] in seen:
            if verbose:
                print ("Removing duplicate bond {:6d} -- {:<6d}({})".
                    format(bond[0], bond[1], offset))
        else:
            seen.add(bond)
            seen_bonds.append((bond, offset))
    print ("Removed {} duplicates, {} left to oxidise.".
        format(len(bonds_to_oxidise)-len(seen), len(seen)))
    bonds_to_oxidise = seen_bonds
        
    # 3. Add oxygen in the middle of the bonds
    print ("mincorerad", mincorerad)
    for indices, offset in bonds_to_oxidise:
        at1, at2 = atoms[indices[0]], atoms[indices[1]]
        if at1.symbol == at2.symbol == aS:
            posO = 0.5 * (at1.position + at2.position + np.dot(cell, offset))
            # do not add oxygen if its coordinates violate the
            # minimum core-Si radius
            if (posO[1]**2 + posO[2]**2) > mincorerad**2:
                atoms.append(Atom(aO, posO))
                atO = atoms[len(atoms)-1]
                if verbose:
                    print("Added {:s}, mid-bond {:6d} -- {:<6d}{:12.3f}".
                        format(verbose_atom(atO), at1.index, at2.index,
                               (posO[1]**2+posO[2]**2)**0.5 - mincorerad))
            else:
                if verbose:
                    print("Dropping {:s}, mid-bond {:6d} -- {:<6d} {:12.3}(violates core radius)".
                        format(verbose_atom(atO), at1.index, at2.index,
                            (posO[1]**2+posO[2]**2)**0.5 - mincorerad))

    # 3c. Prune overlapping atoms. It turns out that due to relaxation
    #     we end up with (reconstructed) surface atoms that are bonded 
    #     together, or even inside the oxide we may get partially
    #     oxidised atoms that are bonded to each other. The above steps
    #     lead to such Si-Si bonds being oxidised twice.
    #     Therefore, here we check for overlapping O atoms and prune them.
    #     By overlapping, we mean not exactly, but within overlap_radius
    #     from each other.
    overlapatoms = []
    for at1 in atoms:
        for at2 in atoms:
            # Any couple of O atoms with coord. differing by less than
            # overlapadius. will be treated as duplicates.
            if at2.index > at1.index and\
                at1.symbol == at2.symbol == aO and\
                interatomic_dist(at1, at2, None, None) < overlap_radius:
#                np.allclose(at1.position, at2.position, atol=overlap_radius):
                if verbose:
                    print("Found overlapping oxygens: {:n}, {:n}".
                            format(at1.index, at2.index))
                overlapatoms.append(at2.index)
    overlapatoms = sorted(set(overlapatoms))
    if overlapatoms:
        if verbose:
            print("Pruning duplicate O atoms: {}".format(tostr(overlapatoms)))
        del atoms[overlapatoms]

    nOstop = len([at for at in atoms if at.symbol==aO])
    print("Added {:n} {:s} atoms in total".format(nOstop-nOstart, aO))

    return atoms.copy()
