"""Helper routines to generate Si structures
"""
from __future__ import print_function
from ase.lattice import bulk
from ase.io import write
from math import pi, sqrt, ceil
import numpy as np
from poxy.utils.misc import center_at_origin
from poxy.utils.misc import setup_logger

aSi = 5.4315 # quilibrium lattice constant at 300K [ref?]

def log_unsupported(*args, **kwargs):
    errstr = "Not supported yet."
    try:
        kwargs['logger'].error(errstr)
    except KeyError:
        print (errstr)
    exit(1)

def Si_film(rep=(2,2,4), **kwargs):
    """Generate unreconstructed and unpassivated Si film with desired dimensions.

    Returns:
        ASE Atoms object
    """
    atoms = bulk('Si', 'diamond', a=5.4315, cubic=True)
    film = atoms.repeat(rep)
    newcell = film.get_cell()
    vz = newcell[2][2]
    newcell[2][2] = vz*2
    film.set_cell(newcell)
    return film

def nw_100(D, L, tvac=25., aSi=aSi, center='cell', shift=0., **kwargs):
    """
    """
    # get an 8-atom cubic unit cell of Si
    atoms = bulk('Si', 'diamond', a=aSi, cubic=True)

    # make up a principle layer -- a repeat unit along the wire axis
    rep_d = int(ceil(float(D)/aSi))
    rep_l = int(ceil(float(L)/aSi))
    pl = atoms.repeat((1, rep_d, rep_d))

    # center the atoms around the origin
    # NOTE: If we do not apply a small correction on the 'center' point,
    #     we end up with an asymmetric NW cross-section. By default however,
    #     we want a symmetric NW cross-section, so we shift by 1/8 of the 
    #     diagonal of the cubic cell: aSi*sqrt(2)/8, in Y direction only!
    shift_0 = np.array([0., -aSi*sqrt(2)/8., -aSi*sqrt(2)/8.])
    center_at_origin(pl, about=center, shift=(shift + shift_0))

    # cut-out a circular cross-section
    r = float(D)/2.
    print ("NW radius = {} Angstroms".format(r))
    del pl[[at.index for at in pl if at.y**2 + at.z**2 > r**2]]

    # fix vacuum buffer
    a, b, c = [v[i] for i,v in enumerate(pl.get_cell())]
    ymin = min([at.y for at in pl])
    ymax = max([at.y for at in pl])
    zmin = min([at.z for at in pl])
    zmax = max([at.z for at in pl])
    b = ymax - ymin + tvac
    c = zmax - zmin + tvac
    pl.set_cell([[a, 0, 0], [0, b, 0], [0, 0, c]])
    center_at_origin(pl, about=center, shift=0.)
    print ("PL of {}-atoms\n".format(len(pl)), pl)

    # extend along nanowire length
    nw = pl.repeat((rep_l, 1, 1))
    print ("NW of {}-atoms\n".format(len(nw)), nw)

    return nw
    
def nw_110(D, L, tvac=25., aSi=aSi, center='cell', shift=0., **kwargs):
    """Create a semi-circular [110]-oriented Si (NW) with diameter *D* and 
    length *L* along [110] direction, i.e. *x* axis is oriented along [110].
    Add a *tvac* vacuum buffer in transverse directions.
    Optionally, modify the equilibrium lattice constant of Si, using *aSi*.
    L, D, tvac, and aSi are in [Angstroms].

    Parameters:
        D, L : float
            Diameter and length of the nanowire [A]
        tvac : float, optional
            Thickness of the vacuum buffer [A] (optional). Default value seems 
            to yield lowest pressure when minimization of Si film is done with
            Tersoff_SiO potential in lammps. But it should likely be optimised
            for each system and application.
        aSi : float, optional
            Lattice constant of Si (optional) [A]
        center : string in {'cell': (Default) unit cell, 'COM': center of mass}, 
            or a 3D-vector-like.
            Translate the center of the atomic structure at the origin.
            The center is explicitly specified as a 3D-vector or a string.
        shift : 3d vector-like or a float
            Shift added to the selected *center*. It is useful to generate NWs
            with asymmetrical cross-section, if so needed.

    Returns:
        nw: Atoms object
            A Si NW with the closest possible D and L, with a semi-circular 
            cross-section. All atoms that cannot fit in a circle of diameter 
            *D* are excluded.  Note that the circle is centered at the 
            center-of-mass, and that may lead to some very small asymmetry 
            in the cross-section.
            The structure is periodic in all three directions.
    """

    # get an 8-atom cubic unit cell of Si
    atoms = bulk('Si', 'diamond', a=aSi, cubic=True)
    # NOTABENE: rotate_cell=True below means that future
    #           repetition of the cell will happen along
    #           the original cubic cell axes
    atoms.rotate('z', a=pi/4, rotate_cell=True)
    # atoms.write("uc.gen")
    
    # make up a principle layer -- a repeat unit along the wire axis
    # NOTABENE: 1. the repeat unit along <110> is longer than aSi along <100>.
    #              Its length is equivalent of the diagonal of the cubic cell,
    #              hence the sqrt(2) factor below.
    #           2. recall that cell vectors are still along <100>
    a_110 = sqrt(2)*aSi
    # Extend by 1 the integer rep_***, to ensure we have enough buffer beyond
    # the NW radius for each atomic layer along the NW.
    rep_001 = int(ceil(float(D)/aSi)) + 1
    # Repetition along 010 reduces by sqrt(2) when projected on -110.
    rep_010 = int(ceil(sqrt(2)*float(D)/aSi)) + 1
    # Repetition along 110 has longer unit (the diagonal of the cubic cell).
    rep_110 = int((float(L)/a_110))
    print ("PL in 110 requires the following Si cubic cell repetitions:")
    print ("\t110: {}, 010: {}, 001: {}".format(rep_110, rep_010, rep_001))
    pl = atoms.repeat((1, rep_010, rep_001))
    a, b, c = aSi*sqrt(2), sqrt(2) * rep_010 * aSi, rep_001 * aSi
    pl.set_cell([[a, 0, 0], [0, b, 0], [0, 0, c]])
    
    # fold the atoms in a unit cell that is transverse to [110]
    pl.set_positions(pl.get_positions(wrap=True))
    # pl.write('pl.ortho.uncentered.gen')

    # center the atoms around the origin
    # NOTE: If we do not apply a small correction on the 'center' point,
    #     we end up with an asymmetric NW cross-section. By default however,
    #     we want a symmetric NW cross-section, so we shift by 1/8 of the 
    #     diagonal of the cubic cell: aSi*sqrt(2)/8, in Y direction only!
    shift_0 = np.array([0., -aSi*sqrt(2)/8., 0.])
    center_at_origin(pl, about=center, shift=(shift + shift_0))

    # cut-out a circular cross-section
    r = float(D)/2.
    print ("NW radius = {} Angstroms".format(r))
    del pl[[at.index for at in pl if at.y**2 + at.z**2 > r**2]]

    # fix vacuum buffer
    a, b, c = [v[i] for i,v in enumerate(pl.get_cell())]
    ymin = min([at.y for at in pl])
    ymax = max([at.y for at in pl])
    zmin = min([at.z for at in pl])
    zmax = max([at.z for at in pl])
    b = ymax - ymin + tvac
    c = zmax - zmin + tvac
    pl.set_cell([[a, 0, 0], [0, b, 0], [0, 0, c]])
    center_at_origin(pl, about=center, shift=0.)
    print ("PL of {}-atoms\n".format(len(pl)), pl)

    # extend along nanowire length
    nw = pl.repeat((rep_110, 1, 1))
    print ("NW of {}-atoms\n".format(len(nw)), nw)

    return nw

class StructureFactory(object):
    """
    """
    optional = {'tvac': 40, 'aSi': aSi, 'center':'cell', 'shift': 0.,
                'filename': 'structure.gen'}

    def __init__(self, usrargs):
        """Initialization of the structure.

        Parameters:
            usrargs: aprgparse.args
                command-line input options
        """
        try:
            self.struct_type = usrargs.struct_type
        except AttributeError:
            print ("StructureFactory needs a struct_type!")
        _usrargs = vars(usrargs)
        print (_usrargs)
        for key in self.optional.keys():
            try:
                # this should become an _instance_ variable... though it doesn't matter
                self.optional[key] = _usrargs[key]
            except KeyError:
                pass
        self.atoms = factories[self.struct_type](usrargs, **self.optional).atoms
        self.filename = self.optional['filename']

    def write(self, filename=None):
        if filename is None:
            filename = self.filename
        self.atoms.write(filename)
    

class WireFactory(object):
    """
    """
    def __init__(self, usrargs, **kwargs):
        """
        """
        self.orient = usrargs.orient
        try:
            self.D = usrargs.diameter
        except AttributeError:
            self.D = usrargs.width
        self.L = usrargs.length
        self.atoms = nw_factories[self.orient](
                self.D, self.L, **kwargs) 

factories = {"wire": WireFactory, 
             "dot" : log_unsupported,
             "film": log_unsupported,
             "substrate": log_unsupported}

nw_factories = {"110": nw_110, "100": nw_100, "111": log_unsupported}


def set_create_parser(parser=None):
    """Define parser options specific for structure creation.

    Parameters:

        parser: python parser
            Typically, that will be a sub-parser passed from top executable.
    """
    myname = 'create'
    # Initialise argument parser if not provided
    if parser is None:
        parser = argparse.ArgumentParser(
            description="Generate a Si nano-structure.")
        parser.add_argument(
                "-v", dest="verbose", default=False, action="store_true", 
                help="Verbose output",
                )
        subparser = False
    else:
        subparser = True
    # Once we have a parser, we can add arguments to it
    parser.add_argument(
            "-s","--struct_type", dest="struct_type", default='wire', type=str, action="store", 
            help="Type of atomic structure, one of wire, dot, film, substrate")
    parser.add_argument(
            "-o","--orientation", dest="orient", default='', type=str, action="store", 
            help="Orientation axis(wire) or interface-normal (film, and substrate)")
    parser.add_argument(
            "-l","--length", dest="length", default='10', type=float, action="store", 
            help="Length (A) for wire, film and substrate; ignored for dot")
    parser.add_argument(
            "-d","--diameter", dest="diameter", default='10', type=float, action="store", 
            help="Diameter (A) for wire and dot; "
                 "Interpreted as half-width (A), if mistakenly specified for film or substrate")
    parser.add_argument(
            "-w","--width", dest="width", default='10', type=float, action="store", 
            help="Width (A) for film and substrate;"
                 "Interpreted as diameter (A), if mistakenly specified for wire or dot")
    parser.add_argument(
            "-vac","--vacuum", dest="tvac", default='50', type=float, action="store", 
            help="Vacuum buffer (A) in non-periodic directions")
    parser.add_argument(
            "-aSi","--lattice-const", dest="aSi", default=aSi, type=float, action="store", 
            help="Vacuum buffer (A) in non-periodic directions")
    parser.add_argument(
            "-f", "--filename", dest="outfile", type=str, default="structure.gen", 
            action="store", help="Output file that can be read by lammps read_data command"
            )
    if subparser:
        parser.set_defaults(func=main_create)
        return None
    else:
        return parser

def main_create(args):
    """
    Calls appropriate routine depending on the *args.struct_type*.
    """
    logger = setup_logger('poxy_create', 'poxy_create.dbg.log')

    structure = StructureFactory(args)
    structure.write(args.outfile)

    


#def Si_NW(d, l, orient='110'):
#    """Return a Si nanowire of desired dimensions and orientation
#
#    """
#    # this gets us one cubic unit cell (8-atom)
#    atoms = bulk('Si', 'diamond', a=5.4315, cubic=True)
#    # we have to repeat it sufficient number of times, so that
#    # even when it is re-oriented we can have the desired 
#    # diameter and length.
#    # How to decide the number of repetitions?
#    # suppose we want first 100
##
#    film = atoms.repeat(rep)
#    newcell = film.get_cell()
#    vz = newcell[2][2]
#    newcell[2][2] = vz*2
#    film.set_cell(newcell)
#    return film


