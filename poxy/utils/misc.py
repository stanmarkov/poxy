# Copyright (C) 2015, Stanislav Markov, The University of Hong Kong
# See accompanying license files for details.
from __future__ import print_function
"""
Various utility/helper functions for:
    Reading/Writing files with atomic structures
    Dealing with atomic structures
    Dealing with string representations of aspects of atomic structures
"""
import logging
import numpy as np
from os.path import splitext
from ase import Atoms, Atom
from ase.io import read
from pizza.data import data as rdlmpdata
from math import sqrt

ext_md_dump = 'dump'

def tostr(ll):
    """Given a list *ll* of floats or integers, return a string of
    space separated representations in :8.3f or :d format.

    Parameters:
        ll: list of floats or list of integers,
            Input list.
    Returns:
        string:
            Space separated representation of the items in *ll*.

    TODO: add a parameter to control the format of each value.
    """
    try:
        ss = " ".join(["{:d}".format(item) for item in ll]) 
    except ValueError:
        ss = "".join(["{:8.3f}".format(item) for item in ll])
    return ss

def print_atoms_extent(atoms):
    """Print min/max coordinates in xyz of *atoms*"""
    xmin, xmax = min([at.x for at in atoms]), max([at.x for at in atoms])
    ymin, ymax = min([at.y for at in atoms]), max([at.y for at in atoms])
    zmin, zmax = min([at.z for at in atoms]), max([at.z for at in atoms])
    print ("Atomic configuration of {} atoms with extent:".format(len(atoms)))
    print ("X_min {:10.3f}, X_max {:10.3f}".format(xmin, xmax))
    print ("Y_min {:10.3f}, Y_max {:10.3f}".format(ymin, ymax))
    print ("Z_min {:10.3f}, Z_max {:10.3f}".format(zmin, zmax))

def atomsdatastr(atoms):
    """For every atom prepare a line string with:

    atom.index atom_type_index atom.x atom.y atom.z

    Parameters:
        atoms: ASE Atoms object
    Returns:
        list of strings:
            Each string in the list is formatted in accord to lammps.data file.
            index(int) atom_type_index(int) x(float) y(float) z(float)
    Remarks:
        1) atom_type_indexing follows the order of occurrence of of chemical 
           symbols.
        2) No new line character at the end of the strings in the returned list.
    """
    species = atoms.get_chemical_symbols()
    atomtypes = dict(zip(set(species), range(len(species))))
    datastr = []
    for at in atoms:
        # NOTABENE: lammps wants atom/type indexing from 1 not from 0!
        datastr.append("{:d} {:d} {:f} {:f} {:f}".
            format(at.index+1, atomtypes[at.symbol]+1, at.x, at.y, at.z))

    return datastr

def velocitiesdatastr(atoms):
    """For every atom prepare a line string with:

    atom.index atom.velocity_x atom.velocity_y atom.velocity_z

    Parameters:
        atoms: ASE Atoms object
    Returns:
        list of strings or None (if no velocities in _atoms_):
            Each string in the list is formatted in accord to lammps.data file.
            index(int)  vx(float) vy(float) vz(float)
    Remarks:
        1) No new line character at the end of the strings in the returned list.
    """
    velocities = atoms.get_velocities()
    if velocities is None:
        return None
    datastr = []
    for at, vel in zip(atoms, velocities):
        # NOTABENE: lammps wants atom/type indexing from 1 not from 0!
        datastr.append("{:d} {:f} {:f} {:f}".
            format(at.index+1, vel[0], vel[1], vel[2]))
    return datastr

def center_at_origin(atoms, about='cell', shift=0.):
    """Center the *atoms* at the origin of the coordinate system.

    The coordinates of the *atoms* object are changed.
    The center of the atoms may be specified explicitely by *about*.

    Parameters:
        atoms: ASE Atoms object 
            Atoms, whose coordinates are translated so that the middle
            of the structure is at the origin or shifted from the 
            origin by the specified *shift*.
        about: string of {'cell': Default, Center of the Unit Cell
            'COM': center of mass}, or, 3D-vector-like.
            This is the point which is translated to the origin, and all
            atomic coordinates are translated accordingly.
        shift: 3d-vector-like or a float.
            A shift added to the center point selected via *above*.

    Returns:
        None
    """
    if about == 'cell':
        # the following ASE command centers the atoms inside the supercell.
        # with equal amount of vacuum surrounding the atomic structure.
        # Thereafter, the center of the cell and the center of the atomic
        # structure are assumed the same.
        atoms.center()
        translation = np.sum(-0.5*atoms.get_cell(), axis=0)
    elif about == 'COM':
        translation = - atoms.get_center_of_mass()
    else:
        translation = - about
    atoms.set_positions(atoms.get_positions() + translation + shift)

def write_lammps_data(atoms, filename, elements=None, natomtypes=None, center=True, box=None, writevelocities=True):
    """Write *atoms* to a lammps.data file.

    Parameters:
        atoms: Atoms object (instance)
            Atomic structure.
        filename: string
            Filename to write to.
        elements: None (Default) or list of strings or string
            List of elements that is mapped to the atom types (integers) used
            in the output lammps.data file. If string, them it must contain
            space separated atom symbols (e.g. "Si O")
        center: bool (Default: True)
            Center the atomic structure around the origin.
        box: None (Default) or list of 3 3D-vectors 
            The bounding box; if None, it will be derived from the unit cell
        writevelocities: Bool (Default: True)
            Flag to write (if available) or not the velocities

    Remarks:
        1) Currently it handles only orthogonal box.
        2) Use elementstbl to anticipate addition of elements by other tool
           or routine. E.g. suppose we have pristine Si NW and we want to 
           anticipate addition of O (during oxidation). In this case
           elements = ['Si', 'O'].
    """
    natoms = len(atoms)

    if elements is None:
        if natomtypes is None:
            natomtypes = len(set(atoms.get_atomic_numbers()))
    else: 
        try:
            # in case elements is string of space separated atomic symbols
            _elements = elements.split()
        except AttributeError:
            _elements = elements
        natomtypes = len(_elements)

    # This wrapping back to the unit cell (which has a virtex at 0.0)
    # causes big trouble if the atoms are centered about the origin.
    # So we have to comment the following code until we find a real need
    # or better way.
    # --------------------------------------------------------------------
    # | it may happen that atoms fall outside the bounding box, in periodic direction
    # | and lammps may be unable to assign them correctly, exiting with error.
    # | so we make sure positions are within the box; this must be done before centering
    # | wrapped_positions = atoms.get_positions(wrap=True)
    # | atoms.set_positions(wrapped_positions)
    
    # | Note that this puts the atoms at the center of the unit cell, with equal
    # | amount of vacuum on each side.
    # | atoms.center()
    # | atoms.write('test.atoms.center.gen')
    # | print ("Test atoms.center():")
    # | print_atoms_extent(atoms)
    # --------------------------------------------------------------------

    # if we want to translate the center to origin, here we do it 
    if center:
        center_at_origin(atoms, about='cell')
#    atoms.write('test.atoms.center_at_origin.gen')
#    print ("Test atoms.center_at_origin():")
#    print_atoms_extent(atoms)

    # NOTABENE: This implicitly assumes orthogonal box, and may need to change.
    #           in the future.
    if box is None:
        # The box has to be derived from the unit_cell of the atomic structure.
        # The code assumes orthogonal box.
        cell = atoms.get_cell()
        a,b,c = [np.sqrt(sum([vc**2 for vc in v])) for v in cell]
        if center:
            # this assumes that atoms are right in the middle of the cell
            # and that the cell center is translated to the origin
            xlo, xhi = -a/2., +a/2.
            ylo, yhi = -b/2., +b/2.
            zlo, zhi = -c/2., +c/2.
        else:
            # this only assumes that atoms are right in the middle of the cell.
            xlo, xhi = 0., a
            ylo, yhi = 0., b
            zlo, zhi = 0., c
    else:
        # this is explicitly specified box. but still it is orthogonal only!
        xlo, ylo, zlo = box[0]
        xhi, yhi, zhi = box[1]

    # finally, we are ready to write 
    ll = []

    # NOTABENE: empty lines ARE important. 
    #           and only 1 signle space should separate xlo through zhi
    ll.append("Input data for lammps")
    ll.append("")
    ll.append("{:7d}   atoms".format(natoms))
    ll.append("{:5d}     atom types".format(natomtypes))
    ll.append("{:f} {:f} xlo xhi".format(xlo, xhi))
    ll.append("{:f} {:f} ylo yhi".format(ylo, yhi))
    ll.append("{:f} {:f} zlo zhi".format(zlo, zhi))
    ll.append("")
    ll.append("Atoms")
    ll.append("")

    ll.extend(atomsdatastr(atoms))
    if atoms.has('momenta') and writevelocities:
        ll.append("Velocities")
        ll.append("")
        ll.extend(velocitiesdatastr(atoms))

    with open(filename, 'w') as f:
        f.writelines("\n".join(ll))
        f.writelines("\n")

def read_lammps_dump(fileobj='md.{}'.format(ext_md_dump), elementstbl=None, index=-1, order=True,
        atomsobj=Atoms, center=True):
    """Read dump file from MD and return an ASE atoms object.

    It is based on the read handler from ase.io for lammps, but extends its 
    functionality to derive a unit cell info from the box info in lammps.

    Parameters:
        fileobj: int or string 
            File containing dump data (coordinates and velocities at 
            different timesteps) from MD.
        elementstbl: list of strings (e.g. ['Si', 'O']
            List of chemical symbols, which dictates atomtype mapping to 
            chemical element, in case that *fileobj* does not state them.
            Recall that lammps requires custom dump to output atom species in 
            the dump file from MD.
        index: Which configuration from the dump file to return.
            Note that all configurations are read in the images list, whic
            slows down things significantly, if the dump file has many and 
            large configurations.
        order: Order the particles according to their id. Might be faster to
            switch it off.
        atomsobj: Class (Defalt ASE Atoms)
            A class implementing Atoms object, an instance of which will be returned.
        center: bool (default: True)
            If true, then atoms are translated so that their middle is at the
            origin of the coordinate system.

    Returns:
        atomsobj instance:
            Atoms object, initialised from the selected configuration.
    """

    if isinstance(fileobj, str):
        f = open(fileobj)
    else:
        f = fileobj

    # load everything into memory
    lines = f.readlines()

    natoms = 0
    images = []
    timestep = 0

    while len(lines) > natoms:
        line = lines.pop(0)

        if 'ITEM: TIMESTEP' in line:
            n_atoms = 0
            lo = []
            hi = []
            tilt = []
            id = []
            types = []
            elements = []
            positions = []
            scaled_positions = []
            velocities = []
            forces = []
            quaternions = []
            timestep += 1
            # print ("TIMESTEP: {}".format(timestep))

        if 'ITEM: NUMBER OF ATOMS' in line:
            line = lines.pop(0)
            natoms = int(line.split()[0])
            # print ("Structure of {} atoms".format(natoms))
            
        if 'ITEM: BOX BOUNDS' in line:
            # save labels behind "ITEM: BOX BOUNDS" in
            # for orthogonal box the format is: ITEM: BOX BOUNDS xx yy zz
            # for triclinic  box the format is: ITEM: BOX BOUNDS xy xz yz xx yy zz
            bc_tilt_items = line.split()[3:]
            for i in range(3):
                line = lines.pop(0)
                fields = line.split()
                lo.append(float(fields[0]))
                hi.append(float(fields[1]))
                if (len(fields) >= 3):
                    # only for triclinic box
                    tilt.append(float(fields[2]))

            # determine cell tilt (triclinic case!)
            if (len(tilt) >= 3):
                # for >=lammps-7Jul09 use labels behind
                # "ITEM: BOX BOUNDS" to assign tilt (vector) elements ...
                if (len(tilt_items) >= 3):
                    xy = tilt[tilt_items.index('xy')]
                    xz = tilt[tilt_items.index('xz')]
                    yz = tilt[tilt_items.index('yz')]
                # ... otherwise assume default order in 3rd column
                # (if the latter was present)
                else:
                    xy = tilt[0]
                    xz = tilt[1]
                    yz = tilt[2]
            else:
                # orthogonal box
                xy = xz = yz = 0

            xhilo = (hi[0] - lo[0]) - (xy**2)**0.5 - (xz**2)**0.5
            yhilo = (hi[1] - lo[1]) - (yz**2)**0.5
            zhilo = (hi[2] - lo[2])

            if xy < 0:
                if xz < 0:
                    celldispx = lo[0] - xy - xz
                else:
                    celldispx = lo[0] - xy
            else:
                celldispx = lo[0]
            celldispy = lo[1]
            celldispz = lo[2]

            cell = [[xhilo, 0, 0], [xy, yhilo, 0], [xz, yz, zhilo]]
            celldisp = [[celldispx, celldispy, celldispz]]

            pbc = []
            for bc in bc_tilt_items[-3:]:
                if bc == 'pp':
                    pbc.append(True)
                else:
                    pbc.append(False)

        def add_quantity(fields, var, labels):
            for label in labels:
                if label not in atom_attributes:
                    return
            var.append([float(fields[atom_attributes[label]])
                        for label in labels])
                
        if 'ITEM: ATOMS' in line:
            # (reliably) identify values by labels behind
            # "ITEM: ATOMS" - requires >=lammps-7Jul09
            # create corresponding index dictionary before
            # iterating over atoms to (hopefully) speed up lookups...
            atom_attributes = {}
            for (i, x) in enumerate(line.split()[2:]):
                atom_attributes[x] = i
            for n in range(natoms):
                line = lines.pop(0)
                fields = line.split()
                id.append(int(fields[atom_attributes['id']]))
                try:
                    types.append(int(fields[atom_attributes['type']]))
                except KeyError:
                    pass
                try:
                    elements.append(fields[atom_attributes['element']])
                except KeyError:
                    pass
                add_quantity(fields, positions, ['x', 'y', 'z'])
                add_quantity(fields, scaled_positions, ['xs', 'ys', 'zs'])
                add_quantity(fields, velocities, ['vx', 'vy', 'vz'])
                add_quantity(fields, forces, ['fx', 'fy', 'fz'])
                add_quantity(fields, quaternions, ['c_q[1]', 'c_q[2]',
                                                   'c_q[3]', 'c_q[4]'])

            if order:
                def reorder(inlist):
                    if not len(inlist):
                        return inlist
                    outlist = [None] * len(id)
                    for i, v in zip(id, inlist):
                        outlist[i - 1] = v
                    return outlist
                types = reorder(types)
                elements = reorder(elements)
                positions = reorder(positions)
                scaled_positions = reorder(scaled_positions)
                velocities = reorder(velocities)
                forces = reorder(forces)
                quaternions = reorder(quaternions)

            if not elements and elementstbl is not None:
                # This would be if the dump contains only atom type-indexes, 
                # but no symbols, so we supply as argument the mapping
                for attype in types:
                    elements.append(elementstbl[attype-1])

            if len(quaternions):
                images.append(Quaternions(symbols=elements,
                                          positions=positions,
                                          cell=cell, celldisp=celldisp,
                                          quaternions=quaternions))
            elif len(positions):
                images.append(atomsobj(
                    symbols=elements, positions=positions,
                    celldisp=celldisp, cell=cell, pbc=pbc))
            elif len(scaled_positions):
                images.append(atomsobj(
                    symbols=elements, scaled_positions=scaled_positions,
                    celldisp=celldisp, cell=cell, pbc=pbc))

            if len(velocities):
                images[-1].set_velocities(velocities)
            if len(forces):
                calculator = SinglePointCalculator(0.0, forces,
                                                   None, None, images[-1])
                images[-1].set_calculator(calculator)
        
    # print ('image index {}'.format(index))
    # print ('number of images {}'.format(len(images)))
    atoms = images[index]
    if center:
        center_at_origin(atoms, about='cell')

    return atoms


def read_lammps_data(fileobj, elements, atomsobj=Atoms, center=True):
    """Read lammps.data file and return an *atoms* object.

    The routine is based on the data.py handler from the pizza utilities for 
    lammps, but is made to return ASE Atoms.

    Parameters:
        fileobj: fileobj or filename(string)
            File ID or Filename of the lammps.data file.
        elements: string
            String containing space separatted atomic symbols of the elements
            corresponding to the lammps atom types.
            NOTE: It is critically important to map the atom symbols to the
            actual order of atom types in the lammps.data file. 
            Therefore, one must know what is in the lammps.data file.
        atomsobj: Class (Defalt ASE Atoms)
            A class implementing Atoms object, an instance of which will be returned.
        center: bool (default: True)
            If true, then atoms are translated so that their middle is at the
            origin of the coordinate system.

    Returns:
        an instance of the *atomsobj*

    Remarks: 
        Currently it would handle only one Atoms object and only 
        orthorombic cell.
    """

    lmpdata = rdlmpdata(fileobj)

    natoms = lmpdata.headers['atoms']

    natomtypes = lmpdata.headers['atom types']
    try:
        # assuming that it is a single string of chemical symbols, space separated
        _elements = elements.split()
    except AttributeError:
        # assume it is a list of strings
        _elements = elements
    assert natomtypes <= len(_elements), \
        "Only {:d} elements ({:s}) given while expecting {:d}".\
            format(len(_elements), " ".join(_elements), natomtypes)

    # this would be the only thing present for orthorombic cell,
    # but for triclinic variants there would probably be more info to handle.
    xlo, xhi = lmpdata.headers['xlo xhi']
    ylo, yhi = lmpdata.headers['ylo yhi']
    zlo, zhi = lmpdata.headers['zlo zhi']
    a, b, c = (xhi - xlo), (yhi - ylo), (zhi - zlo)
    

    atoms = []
    for atstr in lmpdata.sections['Atoms']:
        # NOTABENE: atstr is a single string!!!
        # at: index, atom type (from 1), x, y, z
        at = atstr.split()
        chelem, pos = _elements[int(at[1])-1], [float(s) for s in at[2:5]]
        atoms.append(Atom(chelem, pos))
    assert natoms == len(atoms)

    atoms = atomsobj(atoms)
    atoms.set_cell((a, b, c))
    atoms.set_pbc(True)
    
    # it may happen that atoms fall outside the bounding box, in periodic direction
    # and lammps seems unable to assign them correctly, exiting with error.
    # so we make sure positions are within the box; this must be done before centering
#    wrapped_positions = atoms.get_positions(wrap=True)
#    atoms.set_positions(wrapped_positions)

    if center:
        center_at_origin(atoms, about='cell')

    return atoms

def get_atoms(infile, elements=None, atomsobj=Atoms, center=True, index=-1):
    """Read the *infile* using the appropriate routine as per file extension,
    and return an instance of the *atomsobj*, initialised from the selected
    configuration (by specifying *index*).

    Parameters:
        infile: string (file name)
            Input file with atomic configuration(s).
        elements: string 
            String with space separated atomic symbols, which are mapped to 
            atom type indexes. This is needed if *infile* does not explicitly
            state chemical elements.
        atomsobj: Class (Defalt ASE Atoms)
            A class implementing Atoms object, an instance of which will be returned.
        center: Bool (Default True)
            If true, then the center of the atomic structure is translated
            to origin.
        index: int (Default: -1, meaning last configuration in *infile*)
            Select a particular configuration from (potentially many in) *infile*.

    Returns:
       atomsobj instance: 
           Atoms object, initialised from the selected configuration.
    """
    if splitext(infile)[1] in ['.prd', '.dump']:
        # interpret as lammps dump file with our own routine
        atoms = read_lammps_dump(infile, elementstbl=elements, index=index, center=center)
    elif splitext(infile)[1]=='.data':
        # interpret as lammps data file with our own routine
        # NOTABENE: lammps.data file contains only enumerated atom types
        #           without any info of what chemical elements there are
        assert elements is not None,\
            "Please specify a list of chemical elements 'CE1 CE2...' when reading lammps.data file." 
        atoms = read_lammps_data(infile, elements=elements, center=center)
    else:
        # use ase reader
        atoms = read(infile)

    return atoms
    
def setup_logger(name, filename):
    """Return a logger named *name*.

    Define a console handler at INFO level, and a file handler at DEBUG
    level, writing to *filename*.
    """
    # set up logging
    # -------------------------------------------------------------------
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    # console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    # file handler with full debug info
    fh = logging.FileHandler(filename)
    fh.setLevel(logging.DEBUG)
    # message formatting
    formatter = logging.Formatter('%(levelname)7s: %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger

def interatomic_dist(at1, at2, cell, offset):
    """Return the distance between two atoms (ASE Atom instances).

    If the atoms at1 and at2 are part of a periodic structure,
    cell and offset must e supplied.
    Althernatively, use cell=None, offset=None.
    """
    try:
        p1, p2 = at1.position, at2.position + np.dot(cell, offset)
    except TypeError:
        p1, p2 = at1.position, at2.position
    return np.linalg.norm(p1 - p2)

def dist_pt2pt (p1, p2 = [0,0,0]):
    """
    Return the point-to-point distance
    """
    return np.linalg.norm(np.array(p1) - np.array(p2))

def dist_pt2axis (p, axis=([0,0,0], [1,0,0])):
    """Return the point(p)-to-axis distance d, obtained from the vector formulation: 
    d = \|(a-p) - ((a-p).n)n\|
    where 
    a = axis[0], and n is unit vector along axis.
    """
    pt = np.array(p)
    a, b = np.array(axis[0]), np.array(axis[1])
    n = (b-a)/np.linalg.norm(b - a)
    d = np.linalg.norm((a-pt) - np.dot((a-pt), n) * n)
    return d

def dist_pt2plane (p, plane=([0,0,0], [1,0,0], [0,1,0])):
    """
    Return the point(p)-to-plane distance d from the projection of the 
    vector (p-a) on the normal to the plane at a.
    The plane is defined via three non-collinear points. 
    """
    a, b, c = np.array(plane[0]), np.array(plane[1]), np.array(plane[2])
    pt = np.array(p)
    # get the normal vector
    n = np.cross((b-a), (c-a))
    # normalise it
    n = n / np.linalg.norm(n)
    # assume origin lies in the plane
    # then distance from p to the plane is the projection 
    # of p on the normal to the plane
    d = np.dot((p-a), n)
    return d

def dist_from_point(atom, ix, point=[0,0,0]):
    """Return the distance of atoms[ix] from a point -- origin, 
    by default (none other supported currently)."""
    return sqrt(atoms[ix].x**2 + atoms[ix].y**2 + atoms[ix].z**2)

def dist_from_axis(atoms, ix, axis=([0,0,0], [1,0,0])):
    """Return the radial distance of atoms[ix] from an axis -- 
    through origin parallel to x, by default (none other supported yet)"""
    return sqrt(atoms[ix].y**2 + atoms[ix].z**2)

def dist_from_plane(atoms, ix, plane=([0,0,0], [1,0,0], [0,1,0])):
    """Return the distance of atoms[ix] from plane, defined by three points, 
    xy-plane at origin by default (none other supported yet)"""
    return atoms[ix].z

def get_overlapped_atoms(atoms, overlapradius, species=None):
    """Return a list of indeces in *atoms* of atoms that overlap
    other atoms. By overlap we mean within *overlapradius*.

    The overlapping atoms can be removed using del atoms[overlapatoms].
    If we want to restrict only to given chemical element then both atoms
    must have the same symbol == *species*.
    E.g. get_overlapped_atoms(atoms, 1.1, 'O') will return the indices of
    overlapping oxygens only; an overlapping Si-O will not be seen as such!

    REMARKS:
        1. Currently, atoms that overlap accross a unit cell of a 
           periodic structure are not returned. This is to speed up and 
           avoid the necessary build of a neighbour list. In the future
           neighbour list should be supplied may be...
    """
    logger=logging.getLogger('poxy_logger')
    overlapatoms = []
    for at1 in atoms:
        for at2 in atoms:
            # Any couple of O atoms with coord. differing by less than
            # overlapadius. will be treated as duplicates.
            if at2.index > at1.index and\
                interatomic_dist(at1, at2, None, None) < overlapradius and\
                (species==None or at1.symbol == at2.symbol == species):
                logger.warning("Found overlapping {}/{}: {:n}, {:n}".
                    format(at1.symbol, at2.symbol, at1.index, at2.index))
                overlapatoms.append(at2.index)
    overlapatoms = sorted(set(overlapatoms))
    return overlapatoms

if __name__ == "__main__":
    ll = [3.09181324, 8.12345, 23.]
    ll1 = [-230., -3.12357, 78.038]
    ll1 = [-230., -3.12357, 78.038]
    ll2 = [230, 3, 1078]
    ll3 = [30, 300, 8]
    print ("list: {:s}".format(tostr(ll)))
    print ("list: {:s}".format(tostr(ll1)))
    print ("list: {:s}".format(tostr(ll2)))
    print ("list: {:s}".format(tostr(ll3)))
