================================
POXY
================================

`POXY` is a set of command-line tools and Python_ modules intended to help with
molecular dynamics simulation of Si oxidation.
The code is available under `The MIT License`_.

.. _Python: http://www.python.org
.. _The MIT license: https://opensource.org/licenses/MIT

Contents
================================

.. toctree::

   about
   commands
   examples
   tutorial
   poxylib
   dependencies
   install
   license
   releasenotes




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

