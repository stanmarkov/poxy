.. highlight:: bash

.. index:: Examples

.. _examples:

====================
Examples
====================

A complete tutorial, starting from pristine Si nanowire to the 
movie of oxidation is presented 
:ref:`here <tutorial>`.
The corresponding directory with the necessary input files is in
`path_to_poxy/examples/nw_110_D15_L8`. 
(Downloadable tarball here: 
:download:`nw_110_D15_L8.tar <static/nw_110_D15_L8.tar>`).


Below, a few details concerning the approach.

An example :download:`bash-script <./static/oxidise.sh>` (`oxidise.sh`)
is currently used to handle the flow of incorporating a desired number 
of oxygen atoms within a given atomic structure (Si, or Si/SiO2).
It may be replaced by a `poxy` command in the future.

The script is invoked with three command-line arguments:

1. Input atomic structure;
2. Number of O atoms to be added;
3. (optional) type of bonds (surface/subox/either) to consider when incorporating oxygen.

Note that currently the type of structure is hard-coded as 'wire', but could be 
easily modified along with other tunings for a particular oxidation run.

An example invocation will be:

.. code:: bash

        ./oxidise.sh output/heat.prd 100 either 2>&1 | tee oxidise.log

It assumes ``output/heat.prd`` exists (e.g. from heating up pristine Si NW to 
desired temperature). The output and log files from the MD run will be
placed in ``output`` directory, the structure files and png-images will be
placed in the ``structures`` directory.
The MD engine is lammps.

The script realizes the flow depicted in the :ref:`diagram <diagram>` at the bottom, 
and takes care of the cases, where O cannot be added due to the 
potential overlap of atoms, or an atom is lost during MD due to 
interaction being too strong.
In either case, the cure is some more MD at the selected annealing temperature.

If all goes well, at the end of the run, a video will be produced, illustrating
the individual incorporation and anneal of each O atom, as below:


.. pay attentions to the _static folder being used below, or else mp4 file is not found

.. raw:: html 

        <video width="320" height="333" controls>
                <source src="_static/MD.oxidation.mp4" type="video/mp4">
                Your browser does not support the video tag.
        </video>

An atom-by-atom incorporation of O, followed by equilibration anneal,
leading to layer-by-layer oxidation of the Si-NW.

   
The flow diagram mentioned above:

.. _diagram:

.. figure:: ./static/oxidise-multiple.png
        :scale: 35 %

        Implementation of oxidation flow, one atom at a time with
        safe handling of situations where MD looses atom or O cannot
        be safely incorporated in a specific bond.
