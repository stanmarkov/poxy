.. index:: about

.. _about:

==========
About
==========

`Poxy` consists of a small library and a few command-line routines,
intended to help with the technicalities of performing classical 
molecular dynamics (MD) simulations of oxidation of silicon nanostructres,
e.g. Si nanowires (SiNW), Si films/membranes or Si nanoclusters (SiNC).

`Poxy` serves the apparently simple tasks to:

1. Read an input Si or SiO_2 structure, incorporate an oxygen atom 
   (or many O atoms) and write out the resulting structure.

2. Convert and visualize between few different formats related 
   to input/output of MD simulations.

3. Iterate over 1, until given number of O atoms is incorporated.

Note that the MD simulator is an external engine, and only `LAMMPS`_ is
currently supported (a limitation owing to point 2 above).

`Poxy` assumes the use of non-reactive force-field -- e.g. the Tersoff potential
with `Munetoh`_'s parameterization or the Stillinger-Weber potential as 
extended by `Watanabe`_, suitable for the Si/SiO2 system as occurring in 
thermal oxidation of Si.

Two approaches to consider are:

1. Make bulk amorphous SiO2 structure, cut-out the desired core-shape,
   replace with crystalline Si, anneal to cure the interface [`Illinov`_].

2. Take a crystalline Si structure, incorporating O in the middle of 
   the Si-Si bonds at the interface, repeat until SiO2 of desired thickness
   forms [`Watanabe`_, `Munetoh`_].

`Poxy` currently focuses on the second approach. 
The details of O incorporation are described in `Ganster`_ and `Torre`_, and the concept is illustrated in the two figures below. It models the reaction of O with Si only, ignoring aspects of O transport to the interface.

.. figure:: ./static/mori.iedm.13--Fig7-OxidationFlow.png
        :scale: 75 %

        Oxidation flow by consecutive addition of O between the Si-Si
        bonds at the interface, leading to layer-by-layer oxidation [`Mori`_].

.. figure:: ./static/torre.jap.02--Fig1--SchematicOfOxygenIncorporation.png
        :scale: 30 %

        Selection of position for O incorporation, setting the nominal 1.6 A Si-O 
        distance before the anneal [`Torre`_].

`Poxy` reflects the learning effort of the author (S.Markov) in this field with 
the aim to produce Si nanostructures with native oxide, suitable to study 
electron and phonon transport, carrier dynamics due to light excitation etc.,
in such nanostructures. 
It was jump-started by the kind donation of some prior code by 
Dr. Alessandro Pecchia (Univ. Torr Vergata, Roma, Italy).


.. _Watanabe: http://www.sciencedirect.com/science/article/pii/S0169433204006373
.. _Munetoh: http://www.sciencedirect.com/science/article/pii/S0927025606002023
.. _Illinov: http://dx.doi.org/10.1063/1.4868318
.. _Ganster: http://dx.doi.org/10.1016/j.tsf.2009.09.144 
.. _Torre: http://dx.doi.org/10.1063/1.1489094
.. _Mori: http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6724564
.. _LAMMPS: http://lammps.sandia.gov/
