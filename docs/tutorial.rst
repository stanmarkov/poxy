.. highlight:: bash

.. index:: Tutorial

.. _tutorial:

================================
Tutorial
================================

We start with the creation of a pristine [110]-SiNW with 15 A diameter and
about 8 A long - this is the minimal possible length along this direction.
Below, we add 30 A vacuum buffer to the simulation box.
As an example, the lattice constant of Si is modified to 5.45 A, to 
reflect the relatively high temperature that will be used during oxidation
(1200 K).

.. code::

        poxy create -s wire -o 110 -d 15 -l 8 -vac 30 -aSi 5.45 -f nw_110_D15_L8.gen
        poxy_view "-r 90y" '-c {"Si":1.385}' nw_110_D15_L8.gen

The structure, of 68 Si atoms, is shown below.

.. figure:: static/nw_110_D15_L8.png
        :scale: 35%

        Pristine SiNW (segment) as created.

We are going to heat the structure by MD to the annealing temperature of the 
oxide, say 1200 K, and start adding oxygen atoms.

Before doing that we should organise things a bit -- the addition of an O atom
will yield 4 files: a log file with thermodynamic info from MD (.log), 
a dump file with geometry snapshots at different times (.dump), as well as
a structure file (.gen) and a corresponding image (.png) of the 
equilibrated structure after the addition of an O atom.
The log and dump files go to `output` directory, the structure and image go
to `structures` directory.

.. code::

        mkdir output structures
        /bin/mv nw_110_D15_L8.* structures/


.. NOTE::

        The two scripts used in this tutorial: `prepare.sh` and `oxidise.sh`
        both assume certain invokation of LAMMPS. The assumption is
        encoded in the following variable: ``mdcmd='mpirun -n 4 lmp_g++'``,
        which should be modified as per your case.


To heat the structure to the annealing temperature, we run MD with 
the help of a very short script (:download:`prepare.sh <static/prepare.sh>`):
It requires that the input structure is `pristine.gen`, hence:

.. code::

        cp structures/nw_110_D15_L8.gen structures/pristine.gen
        ./prepare.sh

At this stage, we have `heat.dump` in `output` and `heat.png` shown
below, and we can start incorporating oxygen.

.. figure:: static/heat.png
        :scale: 35 %

        Geometry snapshot at 1200K (`structures/heat.gen`)

To oxidise this NW, we try to incorporate e.g. an equivalent number of atoms,
e.g. 68. The script (:download:`oxidise.sh <static/oxidise.sh>`) is invoked
as follows and runs for short-while (~5min on 4-core dell optiplex).

.. code:: 

        ./oxidise.sh output/heat.dump 68 either 2>&1 | tee oxidise.log
        
Other than the results in `structures`, the script automatically combines
the images into a video (`oxidation.mp4`), shown below.


.. raw:: html 

        <video width="320" height="333" controls>
                <source src="_static/tutorial.video.mp4" type="video/mp4">
                Your browser does not support the video tag.
        </video>

.. note::

        The :download:`prepare.sh <static/prepare.sh>` and 
        :download:`oxidise.sh <static/oxidise.sh>` assume `LAMMPS`_ as an MD
        enjine. Therefore, the corresponding input files must be present
        in the project directory. These are: 
        :download:`md.heat.in <static/md.heat.in>`, and 
        :download:`md.equilibrate.in <static/md.equilibrate.in>`.
        The details pertaining to the MD are beyond the skope of this 
        tutorial.

        The final structure is not ready for electronic structure calculations.
        It must be further relaxed and minimised. Probably it will 
        produce gap-states too. The further relaxation, minimisation and
        eventual passivation of defects is outside the scope of `POXY`.
        Note however, that certain parameters of `md.equilibrate.in` may largely
        affect the final result. Their optimisation is on-going.

.. _LAMMPS: http://lammps.sandia.gov/
