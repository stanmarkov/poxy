.. index:: dependencies

.. _dependencies:

====================
Dependencies
====================

The following programs and utilities are assumed available (on the ``$PATH``/``$PYTHONPATH``):

* `Python`_ 2.7 It may work with Python 3 but is untested.
* `numpy`_ For all those arrays...
* `LAMMPS`_ The MD simulator.
* `pizza.py`_ Python utilities for LAMMPS (specifically the data.py and log.py).
* `ASE`_ Atomistic simulation environment, for the wonderful Atoms object, for example.
* `dftbutils`_ For some structure format conversion, visualisation, and other small bits and bops.
* `povray`_ For ray-traced images of the atomic structures.
* `ffmpeg`_ For making moving pictures out of the above.

.. _Python: http://www.python.org
.. _numpy: http://www.numpy.org/
.. _LAMMPS: http://lammps.sandia.gov/
.. _pizza.py: http://pizza.sandia.gov/
.. _ASE: https://wiki.fysik.dtu.dk/ase/index.html
.. _dftbutils: https://bitbucket.org/stanmarkov/dftbutils
.. _povray: http://www.povray.org/
.. _ffmpeg: https://www.ffmpeg.org/

