#!/bin/bash

module load openmpi-x86_64

echo "Preparing MD input data"
poxy_structure -e "Si O" ./structures/pristine.gen heat.data
echo "Running MD (check structures/heat.log)"
OMP_NUM_THREADS=4; mpirun -n 4 lmp_g++ -in md.heat.in -sc none
echo "Doing post-MD stuff"
poxy_structure heat.dump heat.gen
poxy_view "-r 90y" '-c {"Si":1.385, "O":0.85}' heat.gen heat.png
/bin/mv -f heat.png ./structures/
/bin/mv -f heat.gen ./structures/
nstart=`grep -A 1 'NUMBER OF ATOMS' heat.dump | tail -1` # this is for .dump file
/bin/mv -f heat.dump ./output/
/bin/mv -f heat.log  ./output/
/bin/cp ./structures/heat.png ./structures/add.${nstart}.png

/bin/rm head.data log.lammps
