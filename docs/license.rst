.. index:: license

.. _license:

=======
License
=======

`POXY` is distributed under `The MIT License`_.

To do anything useful with it, one must have working :ref:`dependencies`.
These are freely distributed under GPL or LGPL licenses, 
and are not part of `Poxy`'s code base.

.. _The MIT license: https://opensource.org/licenses/MIT
