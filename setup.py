#!/usr/bin/env python

# Copyright (C) 2015, 2016  Stanislav Markov, The University of Hong Kong
# Please see the accompanying LICENSE file for further information.

from __future__ import print_function
from distutils.core import setup, Command
from distutils.command.build_py import build_py as _build_py
from glob import glob
from os.path import join

import os
import sys

if sys.version_info < (2, 7, 0, 'final', 0):
    raise SystemExit('Python 2.7 or later is required!')

short_description = 'POXY is a helper in Si oxidation by Classical Molecular Dynamics',
long_description  = open('README.txt').read(),
name         = 'poxy'
# todo: try to regenerate ./poxy/__init__.py to include the version stated here
version      = '0.1.3'
package_dir  = {'poxy': 'poxy'}
packages     = []
for dirname, dirnames, filenames in os.walk('poxy'):
        if '__init__.py' in filenames:
            packages.append(dirname.replace('/', '.'))
package_data = {}

scripts=[
         'bin/poxy', 
         'bin/poxy_oxidise', 
         'bin/poxy_structure',
         'bin/poxy_view']

## try to cater for windows
if 'sdist' in sys.argv or os.name in ['ce', 'nt']:
    for s in scripts[:]:
        scripts.append(s + '.bat')

# data_files needs (directory, files-in-this-directory) tuples
data_files = []

setup(name=name,
      version=version,
      description=short_description,
      long_description=long_description,
      url='https://bitbucket.org/stanmarkov/poxy',
      maintainer='Stanislav Markov, The University of Hong Kong',
      maintainer_email='figaro@hku.hk',
      license='MIT',
      platforms=['any'],
      packages=packages,
      package_dir=package_dir,
      package_data=package_data,
      scripts=scripts,
      data_files=data_files,
      classifiers=[
        'Development Status :: 0 - initial/exploratory',
        'Intended Audience :: Developers',
        'Intended Audience :: Education',
        'Intended Audience :: Science/Research',
        'Environment :: Console',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: '
        'MIT',
        'Programming Language :: Python',
        'Topic :: Scientific/Engineering :: Physics/Chemistry'])

